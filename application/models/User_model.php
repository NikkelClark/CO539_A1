<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class User_model extends CI_Model {

		//Get an array of posts by a user
		public function getUserPosts($user) {
			$this->load->database();
			$sql = "SELECT * FROM Messages WHERE user_username = ?";
			$query = $this->db->query($sql, $user);
			return $query->result();
		}

		public function getUsers($id, $name) {
			$data['id'] = ($id == null) ? 1234 : $id;
			$data['name'] = ($name == null) ? "John" : $name;
			return $data;
		}

		//Check if username and password matcb
		public function checkLogin($username, $pass) {
			//Load Core Models
			$this->load->database();
			$this->load->helper('security');

			//Clean user inputs
			$cleanUsername = xss_clean($username);
			$hashPass = xss_clean($pass);
			$hashPass = do_hash($hashPass, 'sha1');

			//Database Query
			$this->db->select('count(*) as count');
			$this->db->from('Users');
			$this->db->where(array(
				'username' => $cleanUsername,
				'password' => $hashPass
			));
			$query = $this->db->get()->result()[0]->count;

			//Return if username & password are correct
			if($query == 1)
                return (boolean)$query;
			return (boolean)$query;
		}

		//Check if the user is following another user
		public function isFollowing($follower, $followed) {
			//load core models
			$this->load->database();
			$this->load->helper('security');

			//Clean user inputs
			$cleanFollower = xss_clean($follower);
			$cleanFollowed = xss_clean($followed);

			//Database Query
			$this->db->select('count(*) as count');
			$this->db->from('User_Follows');
			$this->db->where(array(
				'follower_username' => $cleanFollower, 
				'followed_username' => $cleanFollowed));
			$query = $this->db->get()->result_array()[0];

			//Return if true
			if($query['count'] == true)
				return TRUE;
			return FALSE;
		}

		//Floowing a user
		public function follow($followed) {
			//Cload core models
			$this->load->helper('security');
			$this->load->library('session');
			$this->load->database();

			//Clean user inputs
			$cleanFollowed = xss_clean($followed);

			//Check user is loggedin
			if(($this->session->loggedin == true) && (isset($this->session->username)))
			{
				//Insert Query
				$this->db->insert(
					'User_Follows', array(
						'follower_username' => $this->session->username,
						'followed_username' => $cleanFollowed));
			}
		}
	}
