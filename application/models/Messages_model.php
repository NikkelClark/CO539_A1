<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Messages_model extends CI_Model {

		public function getMessagesByPoster($name) {
			//Load Models
			$this->load->database();

			//Select Query
			$sql = "SELECT * FROM Messages WHERE user_username = ? ORDER BY posted_at DESC";
			$query = $this->db->query($sql, $name);

			return $query->result_array();
		}

		public function searchMessages($string) {
			//Load Models
			$this->load->database();

			//Select Query
			$sql = "SELECT user_username, text, posted_at FROM Messages WHERE text LIKE ? ORDER BY posted_at DESC";
			$query = $this->db->query($sql, "%$string%");

			return $query->result_array();
		}

		public function insertMessage($poster, $string) {
			//Load Models
			$this->load->database();
			$this->load->helper('security');

			//Clean text
			$cleanString = xss_clean($string);

			//Insert Query
			$this->db->insert('Messages', array(
				'user_username' => $poster,
				'text' => $cleanString,
				'posted_at' => date('Y-m-d H:i:s')
				)
			);
		}

		public function getFollowedMessages($name) {
			//Load Models
			$this->load->database();
			$this->load->helper('security');

			//Clean text
			$cleanName = xss_clean($name);

			//Query get followed users
			$sql = "SELECT * FROM User_Follows JOIN Messages ON followed_username = user_username WHERE follower_username = ? ORDER BY posted_at DESC";
			$query = $this->db->query($sql, $cleanName)->result_array();

			return $query; 
		}
	}
