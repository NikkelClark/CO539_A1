<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php echo (isset($title)) ? $title : "{TITLE}"?></title>
	<style>
	nav, h1, form, p {
		text-align: center;
	}
	</style>
</head>
<body>
	<nav>
	  <a href=<?php echo '"'.site_url().'"' ?>>Home</a> |
	  <a href=<?php echo '"'.site_url().'search"' ?>>Search</a> |
<?php echo (isset($this->session->loggedin) && ($this->session->loggedin === true)) ?
	  '<a href="'.site_url().'message">Post Message</a> |' : "";

  	  echo (isset($this->session->loggedin) && ($this->session->loggedin === true)) ? 
	  '<a href="'.site_url().'user/feed/'.$this->session->username.'">My Feed</a> | ' :  "";
	  
	  echo (isset($this->session->loggedin) && ($this->session->loggedin === true)) ? 
	  '<a href="'.site_url().'user/logout">Logout <i>('.$this->session->username.')</i><a/>' : 
	  '<a href="'.site_url().'user/login">Login<a/>';
?>
	</nav>
	<h1><?php echo (isset($h1)) ? $h1 : "{H1 TITLE}"?></h1>
	<p>Enter a keyword you would like to search for</p>
	<form method="GET" action="https://raptor.kent.ac.uk/proj/co539c/microblog/rc495/search/dosearch">
		<input type="text" name="name">
		<button>Search</button>
	</form>
</body>