<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Post</title>
</head>
<style>
	nav, h1, p, form {
		text-align: center;
	}
</style>
<body>
	<nav>
	  <a href=<?php echo '"'.site_url().'"' ?>>Home</a> |
	  <a href=<?php echo '"'.site_url().'search"' ?>>Search</a> |
	  <a href=<?php echo '"'.site_url().'message"' ?>>Post Message</a> |
<?php 
  	  echo (isset($this->session->loggedin) && ($this->session->loggedin === true)) ? 
	  '<a href="'.site_url().'user/feed/'.$this->session->username.'">My Feed</a> | ' : 
	  "";
	  echo (isset($this->session->loggedin) && ($this->session->loggedin === true)) ? 
	  '<a href="'.site_url().'user/logout">Logout <i>('.$this->session->username.')</i><a/>' : 
	  '<a href="'.site_url().'user/login">Login<a/>';
?>
	</nav>	
	<h1>Submit a Post</h1>
	<p>Write a message you want to post and click submit</p>
	<form method="POST" action="https://raptor.kent.ac.uk/proj/co539c/microblog/rc495/message/doPost">
		<textarea type="text" name="text" placeholder="Type your message..."></textarea>
			</br>
		<button>Submit</button>
	</form>
</body>