<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php echo (isset($title)) ? $title : "{TITLE}"?></title>
	<style>
	nav, h1 {
		text-align: center;
	}
	table, tr {
	    border: 1px solid black;
	    border-collapse: collapse;
	}
	th, td {
	    padding: 5px;
	    text-align: left;
	}
	</style>
</head>
<body>
	<nav>
	  <a href=<?php echo '"'.site_url().'"' ?>>Home</a> |
	  <a href=<?php echo '"'.site_url().'search"' ?>>Search</a> |
<?php echo (isset($this->session->loggedin) && ($this->session->loggedin === true)) ?
	  '<a href="'.site_url().'message">Post Message</a> | ' : "";

  	  echo (isset($this->session->loggedin) && ($this->session->loggedin === true)) ? 
	  '<a href="'.site_url().'user/feed/'.$this->session->username.'">My Feed</a> | ' :  "";
	  
	  echo (isset($this->session->loggedin) && ($this->session->loggedin === true)) ? 
	  '<a href="'.site_url().'user/logout">Logout <i>('.$this->session->username.')</i><a/>' : 
	  '<a href="'.site_url().'user/login">Login<a/>';
?>
	</nav>	
	<h1><?php echo (isset($h1)) ? $h1 : "{H1 TITLE}"?></h1>
<?php
	///Creates a follow button
	//Checks if user is loggedin and button may be required 
	if(isset($follow['button']) && ($this->session->username != $name)) 
	{
		//Displays a disabled follow button if user is following that user
		if((isset($this->session->loggedin) == true) && ($follow['data'] === true)) 
		{
			echo '<center><button style="font-weight: bold;" disabled>You are following '.$name.'!</button></center>';
		}
		//Displays a follow button if user isn't following that user
		else if ((isset($this->session->loggedin) == true) && ($follow['data'] != true))
		{
			echo '<center><button onclick="window.location.href=\''.site_url().'user/follow/'.$name.'\';" >Follow '.$name.'</button></center>';
		} 
	}
	//Displays a disabled follow button saying you can't follow yourself
	else if((isset($this->session->loggedin) == true) && isset($name) && ($this->session->username == $name))
	{
		echo '<center><button style="font-weight: bold;" disabled>You can not follow yourself!</button></center>';
	}
?><br/>
<table align="center">
  <tr>
    <th>User</th>
    <th>Message</th>
    <th>Post at</th>
  </tr>
		<?php
		foreach ($posts as $post) {
			echo "
				<tr>
					<td><a href='https://raptor.kent.ac.uk/proj/co539c/microblog/rc495/user/view/".$post['user_username']."'>".$post['user_username']."</a></td>
					<td>".$post['text']."</td>
					<td>".$post['posted_at']."</td>
				</tr>";
		}		
		?>
</table>

</body>
</html>
