<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Login</title>
	<style type="text/css">
	nav {
		text-align: center;
	}
		.Container {
			position: relative;
			width: 300px;
			left: 50%;
			margin-left: -150px;
			border: solid;
			border-width: 1px;
		}

		h1 {
			text-align: center;
		}
		form {
			position: relative;
			left: 50%;
			margin-left: -107.5px;
			width: 215px;
		}

		form ul {
			padding: unset;
		}

		form li {
			list-style: none;
			width: 215px;
			margin-bottom: 5px;
		}

		form li input {
			float: right;
			display: inline;
            width: 120px;
		}

		form button {
			margin-top: 5px;
			margin-left: 25%;
			width: 50%;
			align-items: center;
		}
	</style>
</head>
<body>
	<nav>
	  <a href=<?php echo '"'.site_url().'"' ?>>Home</a> |
	  <a href=<?php echo '"'.site_url().'search"' ?>>Search</a> |
<?php echo (isset($this->session->loggedin) && ($this->session->loggedin === true)) ?
	  '<a href=<"'.site_url().'message">Post Message</a> |' : "";

  	  echo (isset($this->session->loggedin) && ($this->session->loggedin === true)) ? 
	  '<a href="'.site_url().'user/feed/'.$this->session->username.'">My Feed</a> | ' :  "";

	  echo (isset($this->session->loggedin) && ($this->session->loggedin === true)) ? 
	  '<a href="'.site_url().'user/logout">Logout <i>('.$this->session->username.')</i><a/>' : 
	  '<a href="'.site_url().'user/login">Login<a/>';
?>
	</nav>	
		<br/>
	<div class="Container">
		<h1>Login</h1>
<?php
if($this->session->loginError != null)
{
	echo '<h5 style="color: red; text-align: center;">'.$this->session->loginError.'</h5>';
	$this->session->loginError = null;
}
?>
		<form action="doLogin" method="POST">
			<ul>
				<li>Username: <input type="text" name="username" placeholder="Username"></li>
				<li>Password: <input type="password" name="password" placeholder="Password"></li>
				<button type="submit" >Login</button>
			</ul>
		</form>
	</div>
</body>
</html>
