<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class User extends CI_Controller {

		public function __construct() {
			parent::__construct();
			//Load core libraries
			$this->load->library('session');
			$this->load->helper('url');
		}

		public function index() {
			//Redirect users
			header('Location: feed/'.$this->session->username.'');
		}

		///View a users post
		public function view($name = null) {
			//Check is $name is not null
			if($name != null) {
				//Load core models
				$this->load->model('Messages_model');
				$this->load->model('User_model');

				//Database Queries
				$this->data['Title'] = "View Users Post";
				$this->data['posts'] = $this->Messages_model->getMessagesByPoster($name);
				$this->data['follow'] = array('button' => TRUE, 'data' => $this->User_model->isFollowing($this->session->username, $name));
				$this->data['name'] = $name;
				
				//View variables
				$this->data['title'] = "Micro Blog - $name Messages";
				$this->data['h1'] = "$name's Messages";

				//Load views
				$this->load->view('ViewMessages', $this->data);
			} else {
				//Redirect
				header('Location: '.site_url().'');
			}
		}

		///Logout deletes session
		public function logout() {
			//Delete the session varible
			$this->session->sess_destroy();
			header('Location: '.site_url().'user/login');
		}

		///Login screen
		public function login() {
			if($this->session->loggedin === true)
				header('Location: feed/'.$this->session->username.'');

			//Load view
            $this->load->view('Login');
		}

		///Create user session
		public function doLogin() {
			//Load Models
            $this->load->helper('security');
            $this->load->model('User_model');

            //Assign Post Input Data
			$username = $this->input->post('username');
			$password = $this->input->post('password');

			//Assigns a user session if checkLogin is true
            if($this->User_model->checkLogin($username, $password)) {
                //Assign data to user session
                $this->session->loggedin = true;
                $this->session->username = $username;

                //Redirect User
                header('Location: '.site_url().'user/view/'.$this->session->username);
            } else {
            	//Assign Error message
            	$this->session->loginError = "Incorrect Username & Password";

            	//Redirect user
            	header('Location: '.site_url().'user/login');
            }

		}

		///Follows a user
		public function follow($followed = null) {
			//Load Models
            $this->load->model('User_model');

            //Follow user
            if($followed != null) {
            	$this->User_model->follow($followed);
            	header('Location: '.site_url().'user/view/'.$followed);
            }

		}

		///Shows a users feed
		public function feed($name = null) {
			//Shows the feed of thus user
			if($name != null) 
			{
				//Load core models & page Variables
				$this->load->model('Messages_model');
				$this->data['title'] = "Micro Blog - $name Feed";
				$this->data['h1'] = "$name's Feed";

				//Database Queries
				$this->data['posts'] = $this->Messages_model->getFollowedMessages($name);
				
				//Load views
				$this->load->view('ViewMessages', $this->data);
			} 
			//Shows the feed of logged in user
			else 
			{
				header('Location: '.site_url().'user/feed/'.$this->session->username);
			}
		}
	}