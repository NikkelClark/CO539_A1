<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Search extends CI_Controller {

		public function __construct() {
			parent::__construct();
			$this->load->library('session');
			$this->load->helper('url');
		}

		public function index()
		{
			//View variables
			$this->data['title'] = "Micro Blog - Search";
			$this->data['h1'] = "Search";

			//Load the default search view
			$this->load->view('Search', $this->data);

		}

		public function dosearch() {
			//Assigns the string
			$string = $_GET['name'];

			//Check if the input is null or empty string
			if ($string != null) {
				//Load Models
				$this->load->model('Messages_model');

				//Database Queries
				$this->data['posts'] = $this->Messages_model->searchMessages($string);

				//View variables
				$this->data['title'] = "Micro Blog - Search for $string";
				$this->data['h1'] = "Posts relating to '$string'";

				//Load View
				$this->load->view('ViewMessages', $this->data);
			}
		}

	}