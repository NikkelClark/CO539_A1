<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Message extends CI_Controller {

		public function __construct() {
			parent::__construct();
			$this->load->library('session');
			$this->load->helper('url');
		}

		public function index() {
			//Checks if the user is loggedin
			if($this->session->loggedin == true) {
				$this->load->view('Post');
			} else {
				header('Location: https://raptor.kent.ac.uk/proj/co539c/microblog/rc495/user/login');
			}
		}

		public function doPost() {
			//Get Input data
			$text = $this->input->post('text');
			$username = $this->session->username;

			//Break if the text is empty
			if($text == "") {
				header('Location: https://raptor.kent.ac.uk/proj/co539c/microblog/rc495/message');
			} elseif ($this->session->loggedin == true) {
				//Load Model and call function
				$this->load->model('Messages_model');
				$this->Messages_model->insertMessage($username, $text);
				header('Location: https://raptor.kent.ac.uk/proj/co539c/microblog/rc495/user/view/'.$username);
			} else {
				//Redirect user to login
				header('Location: https://raptor.kent.ac.uk/proj/co539c/microblog/rc495/user/login');
			}
		}
	}